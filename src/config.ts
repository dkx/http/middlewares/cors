export declare interface CorsOptions
{
	domains: Array<string>,
	methods?: Array<string>,
	headers?: Array<string>,
	credentials?: boolean,
	maxAge?: number,
}
