import {Middleware, NextMiddlewareFunction, Request, Response, RequestState} from '@dkx/http-server';
import {CorsOptions} from './config';


export function corsMiddleware(cors: CorsOptions): Middleware
{
	return async (req: Request, res: Response, next: NextMiddlewareFunction, state: RequestState): Promise<Response> => {
		state.cors = false;
		res = res.withVaryHeader('origin');

		const origin: string = <string> req.headers.origin;

		if (!origin) {
			return next(res);
		}

		if (
			(cors.domains.length === 1 && cors.domains[0] === '*') ||
			(cors.domains.indexOf(origin) >= 0)
		) {
			res = allow(res, state, cors, origin);

			if (req.method === 'OPTIONS') {
				return res;
			}
		}

		return next(res);
	};
}


function allow(res: Response, state: RequestState, cors: CorsOptions, origin: string): Response
{
	state.cors = true;
	res = res.withHeader('access-control-allow-origin', origin);

	if (typeof cors.methods !== 'undefined') {
		res = res.withHeader('access-control-allow-methods', cors.methods.join(', '));
	}

	if (typeof cors.headers !== 'undefined') {
		res = res.withHeader('access-control-allow-headers', cors.headers.join(', '));
	}

	if (typeof cors.maxAge !== 'undefined') {
		res = res.withHeader('access-control-max-age', cors.maxAge + '');
	}

	if (cors.credentials === true) {
		res = res.withHeader('access-control-allow-credentials', 'true');
	}

	return res;
}
