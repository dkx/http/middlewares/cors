import {testMiddleware, RequestState} from '@dkx/http-server';
import {expect} from 'chai';
import {corsMiddleware} from '../../lib';


describe('#corsMiddleware', () => {

	it('should inject all CORS headers', async () => {
		const state: RequestState = {};

		const res = await testMiddleware(corsMiddleware({
			domains: ['*'],
			methods: ['GET', 'POST', 'OPTIONS'],
			headers: ['a', 'b', 'c'],
			credentials: true,
			maxAge: 1000,
		}), {
			state,
			headers: {
				origin: 'http://localhost',
			},
		});

		expect(state.cors).to.be.equal(true);
		expect(res.headers).to.be.eql({
			'vary': 'origin',
			'access-control-allow-origin': 'http://localhost',
			'access-control-allow-methods': 'GET, POST, OPTIONS',
			'access-control-allow-headers': 'a, b, c',
			'access-control-max-age': '1000',
			'access-control-allow-credentials': 'true',
		});
	});

	it('should allow access when all domains are allowed', async () => {
		const state: RequestState = {};
		await testMiddleware(corsMiddleware({
			domains: ['*'],
		}), {
			state,
			headers: {
				origin: 'http://localhost',
			},
		});

		expect(state.cors).to.be.equal(true);
	});

	it('should allow access when specific domain is allowed', async () => {
		const state: RequestState = {};
		await testMiddleware(corsMiddleware({
			domains: ['http://localhost'],
		}), {
			state,
			headers: {
				origin: 'http://localhost',
			},
		});

		expect(state.cors).to.be.equal(true);
	});

	it('should deny access when specific domain is not allowed', async () => {
		const state: RequestState = {};
		const res = await testMiddleware(corsMiddleware({
			domains: ['http://localhost:1000'],
		}), {
			state,
			headers: {
				origin: 'http://localhost',
			},
		});

		expect(state.cors).to.be.equal(false);
		expect(res.headers).to.be.eql({
			'vary': 'origin',
		});
	});

	it('should stop further middlewares for OPTIONS method', async () => {
		const state: RequestState = {};
		const res = await testMiddleware(corsMiddleware({
			domains: ['http://localhost'],
		}), {
			state,
			method: 'OPTIONS',
			headers: {
				origin: 'http://localhost',
			},
		});

		expect(state.cors).to.be.equal(true);
		expect(res.headers).to.be.eql({
			'vary': 'origin',
			'access-control-allow-origin': 'http://localhost',
		});
	});

	it('should deny access when origin header is not set', async () => {
		const state: RequestState = {};
		await testMiddleware(corsMiddleware({
			domains: ['http://localhost'],
		}), {
			state,
		});

		expect(state.cors).to.be.equal(false);
	});

});
