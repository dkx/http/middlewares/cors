# DKX/Http/Middleware/Cors

CORS middleware for [@dkx/http-server](https://gitlab.com/dkx/http/server).

## Installation

```bash
$ npm install --save @dkx/http-middleware-cors
```

or with yarn

```bash
$ yarn add @dkx/http-middleware-cors
```

## Usage

```js
const {Server} = require('@dkx/http-server');
const {corsMiddleware} = require('@dkx/http-middleware-cors');

const app = new Server;
const corsOptions = {
	domains: ['*']
};

app.use(corsMiddleware(corsOptions));
```

## Options

* `domains`:
	+ Required
	+ Array of allowed domains
	+ If there is only one item and it is `*`, all incoming requests will be allowed
* `methods`:
	+ Optional
	+ Array of allowed methods
	+ Sets the `access-control-allow-methods` HTTP header
* `headers`:
	+ Optional
	+ Array of allowed headers
	+ Sets the `access-control-allow-headers` HTTP header
* `maxAge`:
	+ Optional
	+ Number
	+ Sets the `access-control-max-age` HTTP header
* `credentials`:
	+ Optional
	+ Boolean
	+ Sets the `access-control-allow-credentials` HTTP header